<?php

abstract class Forma
{
    public $tipoDeForma;

    public function imprimeForma()
    {
        $this -> calculaArea();
        echo $this -> tipoDeForma .  ' com Área de: ' . $this -> calculaArea();
    }

    abstract public function calculaArea();
}

class Quadrado extends Forma
{
    public $lado;
    public function __construct(float $varLado)
    {
        $this-> tipoDeForma = 'Quadrado';
        $this-> lado = $varLado;
    }

    public function calculaArea()
    {
        return $this->lado * $this->lado;
    }
}

class Retangulo extends Forma
{
    public $base;
    public $altura;

    public function __contruct($base, $altura)
    {
        $this-> tipoDeForma = "Retangulo";
        $this-> base = $base;
        $this-> altura = $altura;
    }

    public function calculaArea()
    {
        return $this-> base * $this-> altura;
    }
}

echo "/n";

class Triangulo extends Forma
{
    public $cumprimentoBase;
    public $altura;

    public function __construct($cumprimentoBase, $altura)
    {
        $this-> tipoDeForma = "Triângulo";
        $this-> cumprimentoBase = $cumprimentoBase;
        $this-> altura = $altura;
    }

    public function calculaArea()
    {
        return $this-> cumprimentoBase * $this-> altura / 2;
    }
}

$obj2 = new Triangulo(5, 10);
$obj2-> imprimeForma();


$obj = new Quadrado(5);
$obj->imprimeForma();

echo "<br>";

$obj1 = new Quadrado(100);
$obj1->imprimeForma();

echo "<br>";

$obj2 = new Retangulo(5, 10);
$obj2-> imprimeForma();
